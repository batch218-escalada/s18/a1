/* 	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function 
*/

    // Sum

    function getSum(numA, numB){
        let sum = numA + numB;
        console.log('Displayed sum of ' + numA + ' and ' + numB);
        console.log(sum);
    };

    getSum(5,15);

    // Difference

    function getDifference(numC, numD){
        let diff = numC - numD;
        console.log('Displayed difference of ' + numC + ' and ' + numD);
        console.log(diff);
    };

    getDifference(20,5);
        
/* 	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console. 
*/

    // Product

    function getReturnProduct(numE, numF){
        let returnProduct = numE * numF;
        return returnProduct;
    };

    getReturnProduct(50,10);

    let numE = 50;
    let numF = 10;
    console.log('The product of ' + numE + ' and ' + numF);
    
    let product = getReturnProduct(numE, numF);
    console.log(product);

    // Quotient

    function getReturnQuotient(numG, numH){
        let returnQuotient = numG / numH;
        return returnQuotient;
    };

    getReturnQuotient(50,10);

    let numG = 50;
    let numH = 10;
    console.log('The quotient of ' + numG + ' and ' + numH);
    
    let quotient = getReturnQuotient(numG, numH);
    console.log(quotient);
    
/* 	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console 
*/

    function getAreaFormula(pi, radius){
        let areaFormula = pi * radius**2;
        return areaFormula;
    }

    getAreaFormula(3.14, 15);

    let pi = 3.14;
    let radius = 15;
    console.log('The result of getting the area of a circle with ' + radius + ' radius');

    let circleArea = getAreaFormula(pi, radius);
    console.log(circleArea);

/* 	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console. */
	
    function totalAverage(numW, numX, numY, numZ){
        let totalCount = 4;
        let averageFormula = ((numW + numX + numY + numZ)/totalCount);
        return averageFormula;
    }

    totalAverage(20,40,60,80);

    let numW = 20;
    let numX = 40;
    let numY = 60;
    let numZ = 80;
    let totalCount = 4;
    console.log('The average of ' + numW + ', ' + numX + ', ' + numY + ', ' + numZ + ':');

    let averageVar = totalAverage(numW, numX, numY, numZ, totalCount);
    console.log(averageVar);



/* 	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */

    function checkGradeifPassed(myScore, totalScore){
        let myGrade = ((myScore / totalScore) * 100);
        let isPassed = myGrade >= 75;
        return isPassed;
    };

    checkGradeifPassed(38,50);

    let myScore = 38;
    let totalScore = 50;

    console.log('Is ' + myScore + '/' + totalScore + ' a passing score?');

    let isPassingScore = checkGradeifPassed(myScore, totalScore);
    console.log(isPassingScore);